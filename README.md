## DormitorySystem

![IDE](https://img.shields.io/badge/IDE-IntelliJ%20IDEA-brightgreen.svg) ![JDK](https://img.shields.io/badge/Java-8-blue.svg) ![Database](https://img.shields.io/badge/Database-MySQL5.7-lightgrey.svg)

- 毕业设计
- springboot框架
- 付费咨询
微信:271627441

#### 1、项目介绍

该系统拥有三种用户类型：

1. 系统管理员
   * 管理员：查看当前宿舍学生人数、住宿人数、报修数量、空舍数量、查看学生信息、宿管信息、查看楼宇信息、查看公告信息、查看房间信息、查看报修信息、查看调寝信息、访客管理、查看所有用户信息
2. 宿舍管理员
   * 宿舍管理员：查看当前宿舍学生人数、住宿人数、报修数量、空舍数量、查看学生信息、查看楼宇信息、查看公告信息、查看房间信息、查看报修信息、查看调寝信息、访客管理、查看个人信息
3. 学生
   * 查看当前宿舍学生人数、住宿人数、报修数量、空舍数量、查看我的宿舍、申请调宿、申请报修、查看个人信息

#### 2、开发工具

​	后端：idea   
​	前端：vscode

#### 3、项目技术

​	后端框架：Springboot、MybatisPlus
​	前端技术：ElementUI、vue、css、JavaScript、axios

#### 4、环境

jdk8、maven、node、mysql

#### 5、硬件环境

Windows 或者 Mac OS(不挑剔)

#### 6、项目编号：700053
#### 7、配置

sql文件在doc中

1.修改 application.yaml
2.**进入vue  使用控制台打开**   npm install  而后 npm run serve 启动前端

3.启动 SpringbootApplication

4.用户名或密码在数据库中查看三个用户三个表

### 运行效果
![0.png](doc%2Fimg%2F0.png)
![1.png](doc%2Fimg%2F11.png)
![2.png](doc%2Fimg%2F22.png)
![3.png](doc%2Fimg%2F3.png)
![4.png](doc%2Fimg%2F4.png)
![5.png](doc%2Fimg%2F5.png)
![6.png](doc%2Fimg%2F6.png)
